package fr.soleil.tango.server.enumeratedattribute;

import fr.esrf.Tango.DevFailed;
import fr.esrf.Tango.DevState;
import fr.esrf.Tango.DispLevel;
import fr.esrf.TangoApi.DeviceProxy;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.ext.XLogger;
import org.slf4j.ext.XLoggerFactory;
import org.tango.DeviceState;
import org.tango.orb.ServerRequestInterceptor;
import org.tango.server.InvocationContext;
import org.tango.server.ServerManager;
import org.tango.server.annotation.AroundInvoke;
import org.tango.server.annotation.Attribute;
import org.tango.server.annotation.Command;
import org.tango.server.annotation.Delete;
import org.tango.server.annotation.Device;
import org.tango.server.annotation.DeviceProperty;
import org.tango.server.annotation.DynamicManagement;
import org.tango.server.annotation.Init;
import org.tango.server.annotation.State;
import org.tango.server.annotation.StateMachine;
import org.tango.server.annotation.Status;
import org.tango.server.annotation.TransactionType;
import org.tango.server.attribute.IAttributeBehavior;
import org.tango.server.attribute.log.LogAttribute;
import org.tango.server.dynamic.DynamicManager;
import org.tango.server.dynamic.attribute.ProxyAttribute;
import org.tango.server.dynamic.command.ProxyCommand;
import org.tango.utils.CircuitBreakerCommand;
import org.tango.utils.ClientIDUtil;
import org.tango.utils.DevFailedUtils;
import org.tango.utils.SimpleCircuitBreaker;
import org.tango.utils.TangoUtil;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.StringTokenizer;

@Device(transactionType = TransactionType.NONE)
public final class EnumeratedAttribute {
    private static final String STOP_COMMAND_NAME = "Stop";

    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
    private static final String UNKNOWN = "UNKNOWN";
    private static final String CONFIG_ERROR = "CONFIG_ERROR";
    private final Logger logger = LoggerFactory.getLogger(EnumeratedAttribute.class);
    private final XLogger xlogger = XLoggerFactory.getXLogger(EnumeratedAttribute.class);
    private final Map<EnumDynamicAttribute, DeviceState> enumStateMap = new LinkedHashMap<EnumDynamicAttribute, DeviceState>();
    private final Map<String, Pair<Double, Double>> enumMap = new LinkedHashMap<String, Pair<Double, Double>>();
    private final Map<String, Short> enumShortMap = new LinkedHashMap<String, Short>();
    @DynamicManagement
    private DynamicManager dynMgnt;
    @DeviceProperty
    private String attributeName = "0";
    @DeviceProperty
    private double[] enumeratedAccuracyList = new double[0];
    @DeviceProperty
    private String[] enumeratedLabelList = new String[0];
    @DeviceProperty
    private String[] enumeratedStateList = new String[0];
    @DeviceProperty
    private double[] enumeratedValueList = new double[0];
    @DeviceProperty
    private int modulo = 0;
    @DeviceProperty
    private String stopCommandFullName = "";
    @Attribute
    private String executedTimeStamp = UNKNOWN;
    @State
    private DeviceState state;
    @Status
    private String status;
    private DeviceProxy deviceProxy;
    private String deviceName;
    private String[] enumuratedConfigArray;
    private String enumAttributeName;
    private String invocationContext = "";
    private String executedCommand = UNKNOWN;
    private volatile boolean initOK = false;

    /**
     * MAIN
     */
    public static void main(final String[] args) {
        ServerManager.getInstance().start(args, EnumeratedAttribute.class);
    }

    /**
     * check if enumeratedLabelList property length is equal at enumeratedValueList property length
     *
     * @throws DevFailed
     */
    private void checkProperties() throws DevFailed {
        xlogger.entry();
        if (enumeratedLabelList.length != enumeratedValueList.length) {
            DevFailedUtils.throwDevFailed(CONFIG_ERROR,
                    "EnumeratedLabelList and EnumeratedValueList size are different");
        }
        if (enumeratedLabelList.length != enumeratedAccuracyList.length) {
            DevFailedUtils.throwDevFailed(CONFIG_ERROR,
                    "EnumeratedLabelList and enumeratedAccuracyList size are different");
        }
        if (enumeratedLabelList.length != enumeratedStateList.length) {
            DevFailedUtils.throwDevFailed(CONFIG_ERROR,
                    "EnumeratedLabelList and enumeratedStateList size are different");
        }
        if (!stopCommandFullName.isEmpty()
                && StringUtils.countMatches(stopCommandFullName, TangoUtil.DEVICE_SEPARATOR) != TangoUtil.FULL_NAME_NR_SEPARATOR) {
            throw DevFailedUtils.newDevFailed(CONFIG_ERROR, "StopCommandFullName must contains 4 fields");
        }
        xlogger.exit();
    }

    /**
     * init device
     *
     * @throws DevFailed
     */
    @Init(lazyLoading = true)
    @StateMachine(endState = DeviceState.UNKNOWN)
    public void initDevice() throws DevFailed {
        xlogger.entry();
        // create log attribute
        dynMgnt.addAttribute(new LogAttribute(1000, logger));
        checkProperties();
        enumAttributeName = TangoUtil.getAttributeName(attributeName);
        deviceName = TangoUtil.getfullDeviceNameForAttribute(attributeName);
        deviceProxy = new DeviceProxy(deviceName);
        new SimpleCircuitBreaker(new InitCommand()).execute();
        xlogger.exit();
        enumuratedConfigArray = new String[enumMap.size()];
        short i = 0;
        for (final Entry<String, Pair<Double, Double>> entry : enumMap.entrySet()) {
            final String enumName = entry.getKey();
            enumuratedConfigArray[i] = "\"" + enumName + "\" = " + entry.getValue().getLeft() + " +/- "
                    + entry.getValue().getRight() + " : " + enumeratedStateList[i].toUpperCase();
            enumShortMap.put(enumName, i);
            i++;
        }
        initOK = true;
        logger.debug("device correctly initialized");
    }

    /**
     * clear device
     *
     * @throws DevFailed
     */
    @Delete
    public void deleteDevice() throws DevFailed {
        xlogger.entry();
        dynMgnt.clearAll();
        enumStateMap.clear();
        enumMap.clear();
        xlogger.exit();
    }

    /**
     * return time of last change
     *
     * @return executedTimeStamp
     * @throws DevFailed
     */
    public String getExecutedTimeStamp() throws DevFailed {
        xlogger.entry();
        xlogger.exit();
        return executedTimeStamp;
    }

    /**
     * return the selected attribute
     *
     * @return selected attribute
     * @throws DevFailed
     */
    @Attribute
    public String getSelectedAttributeName() throws DevFailed {
        xlogger.entry();
        String selectedAttributeName = UNKNOWN;
        for (final EnumDynamicAttribute attr : enumStateMap.keySet()) {
            if (attr.isSelected()) {
                selectedAttributeName = attr.getName();
                break;
            }
        }
        xlogger.exit();
        return selectedAttributeName;
    }

    @Attribute
    public String getExecutedCommand() throws DevFailed {
        xlogger.entry();
        xlogger.exit();
        return executedCommand;
    }

    /**
     * return state of the device
     *
     * @return state
     * @throws DevFailed
     */
    public DeviceState getState() {
        xlogger.entry();
        try {
            // if not in a enum position, device is MOVING
            state = DeviceState.getDeviceState(deviceProxy.state());
            for (final Entry<EnumDynamicAttribute, DeviceState> entry : enumStateMap.entrySet()) {
                if (entry.getKey().isSelected()) {
                    state = entry.getValue();
                    break;
                }
            }
        } catch (final DevFailed e) {
            // error getting state device may down
            state = DeviceState.UNKNOWN;
        }
        xlogger.exit();
        return state;
    }

    /**
     * set state of device
     *
     * @param state
     */
    public void setState(final DeviceState state) {
        xlogger.entry();
        this.state = state;
        xlogger.exit();
    }

    /**
     * return status of the device
     *
     * @return status
     * @throws DevFailed
     */
    public String getStatus() {
        xlogger.entry();
        if (initOK) {
            StringBuffer sb = new StringBuffer();
            for (final Entry<EnumDynamicAttribute, DeviceState> entry : enumStateMap.entrySet()) {
                sb = new StringBuffer();
                try {
                    if (entry.getKey().isSelected()) {
                        sb.append("- selected value is \"").append(entry.getKey().getName());
                        sb.append("\" with state ").append(entry.getValue().toString());
                        break;
                    }
                } catch (final DevFailed e) {
                    // error on underlying device
                    sb.append("error getting enum value:\n").append(DevFailedUtils.toString(e));
                }
            }
            if (sb.length() == 0) {
                sb.append("- UNKNOWN enum value and state. Moving in-between enum values?");
            }

            sb.append("\n- ").append(deviceName).append(" is ");
            try {
                sb.append(DeviceState.getDeviceState(deviceProxy.state()));
            } catch (final DevFailed e) {
                // error on underlying device
                sb.append("in error:\n").append(DevFailedUtils.toString(e));
            }
            sb.append("\n- Last execution: ").append(invocationContext).append("\n");
            status = sb.toString();
        }
        xlogger.exit();
        return status;
    }

    /**
     * set status of device
     *
     * @param status
     */
    public void setStatus(final String status) {
        xlogger.entry();
        this.status = status;
        xlogger.exit();
    }

    @Attribute(name = "HardwareState")
    public DevState getSubState() throws DevFailed {
        return deviceProxy.state();
    }

    @Attribute(name = "EnumeratedList")
    public String[] getEnumeratedList() {
        return enumuratedConfigArray;
    }

    @Command(name = "GetEnumeratedValue")
    public double getEnumeratedValue(final String enumLabel) {
        return enumMap.get(enumLabel).getLeft();
    }

    @Command(name = "GetEnumeratedLabelList")
    public String[] getEnumeratedLabelList() {
        return enumMap.keySet().toArray(new String[enumMap.size()]);
    }

    /**
     * set enumeratedLabelList property
     *
     * @param enumeratedLabelList
     * @throws DevFailed
     */
    public void setEnumeratedLabelList(final String[] enumeratedLabelList) throws DevFailed {
        xlogger.entry();
        if (enumeratedLabelList.length == 0) {
            DevFailedUtils.throwDevFailed(CONFIG_ERROR, "EnumeratedLabelList is empty");
        }

        this.enumeratedLabelList = Arrays.copyOf(enumeratedLabelList, enumeratedLabelList.length);
        xlogger.exit();
    }

    @Command(name = "GetEnumAttributeName")
    public String getEnumAttributeName() {
        return enumAttributeName;
    }

    /**
     * @param attributeName
     * @throws DevFailed
     */
    public void setAttributeName(final String attributeName) throws DevFailed {
        xlogger.entry();
        if (attributeName.equals("")) {
            DevFailedUtils.throwDevFailed(CONFIG_ERROR, "No attribute defined in AttributeName property");
        } else if (!attributeName.matches(".*/.*/.*/.*")) {
            DevFailedUtils.throwDevFailed(CONFIG_ERROR,
                    "AttributeName property must be a attribute proxy like : my/device/proxy/myAttribute");
        }
        this.attributeName = attributeName;
        xlogger.exit();
    }

    /**
     * set attribute in param at true
     *
     * @param attrName
     * @throws DevFailed
     */
    @Command(name = "ActivateAttribute")
    public void activateAttribute(final String attrName) throws DevFailed {
        xlogger.entry();
        final IAttributeBehavior da = dynMgnt.getAttribute(attrName);
        if (da instanceof EnumDynamicAttribute) {
            ((EnumDynamicAttribute) da).writeValueOnProxy();
        } else {
            DevFailedUtils.throwDevFailed("UNKNOWN ATTRIBUTE", attrName + " is not a valid attribute.");
        }
        xlogger.exit();
    }

    @Attribute(name = "EnumShortValue", displayLevel = DispLevel._EXPERT)
    public short getEnumShortValue() throws DevFailed {
        short enumShortValue = Short.MIN_VALUE;
        for (final EnumDynamicAttribute attr : enumStateMap.keySet()) {
            if (attr.isSelected()) {
                enumShortValue = enumShortMap.get(attr.getName());
                break;
            }
        }
        return enumShortValue;
    }

    public void setEnumShortValue(final short value) throws DevFailed {
        for (final Entry<String, Short> entry : enumShortMap.entrySet()) {
            if (entry.getValue() == value) {
                activateAttribute(entry.getKey());
                break;
            }
        }
    }

    /**
     * @param ctx
     * @throws DevFailed
     */
    @AroundInvoke
    public void aroundInvoke(final InvocationContext ctx) throws DevFailed {
        xlogger.entry();
        final String enumName = ctx.getNames()[0];
        if (!enumName.equals("Init") && !enumName.equals("State") && !enumName.equals("Status")
                && !enumName.startsWith("GetEnum")) {
            switch (ctx.getContext()) {
                case PRE_COMMAND:
                case PRE_WRITE_ATTRIBUTE:
                    executedTimeStamp = DATE_FORMAT.format(new Date());
                    executedCommand = enumName;
                    final StringBuilder sb = new StringBuilder();
                    invocationContext = sb.append(enumName).append(" from ")
                            .append(ServerRequestInterceptor.getInstance().getClientHostName()).append(" ")
                            .append(ClientIDUtil.toString(ctx.getClientID())).toString();
                    logger.info(deviceName + " - " + invocationContext);
                    break;
                default:
                    break;
            }
        }
        xlogger.exit();
    }

    /**
     * set DynamicManager
     *
     * @param dynMgnt
     */
    public void setDynMgnt(final DynamicManager dynMgnt) {
        xlogger.entry();
        this.dynMgnt = dynMgnt;
        xlogger.exit();
    }

    /**
     * set enumeratedAccuracyList property
     *
     * @param enumeratedAccuracyList
     */
    public void setEnumeratedAccuracyList(final double[] enumeratedAccuracyList) {
        xlogger.entry();
        this.enumeratedAccuracyList = Arrays.copyOf(enumeratedAccuracyList, enumeratedAccuracyList.length);
        xlogger.exit();
    }

    /**
     * set enumeratedStateList property
     *
     * @param enumeratedStateList
     */
    public void setEnumeratedStateList(final String[] enumeratedStateList) {
        xlogger.entry();
        this.enumeratedStateList = Arrays.copyOf(enumeratedStateList, enumeratedStateList.length);
        xlogger.exit();
    }

    /**
     * set enumeratedValueList property
     *
     * @param enumeratedValueList
     */
    public void setEnumeratedValueList(final double[] enumeratedValueList) {
        xlogger.entry();
        this.enumeratedValueList = Arrays.copyOf(enumeratedValueList, enumeratedValueList.length);
        xlogger.exit();
    }

    /**
     * set modulo property
     *
     * @param modulo
     */
    public void setModulo(final int modulo) {
        xlogger.entry();
        this.modulo = modulo;
        xlogger.exit();
    }

    /**
     * set stopCommand
     *
     * @param stopCommandFullName
     */
    public void setStopCommandFullName(final String stopCommandFullName) {
        xlogger.entry();
        this.stopCommandFullName = stopCommandFullName.trim();
        xlogger.exit();
    }

    /**
     * circuit breaker pattern for initializing the device
     *
     * @author ABEILLE
     */
    private class InitCommand implements CircuitBreakerCommand {

        @Override
        public void execute() throws DevFailed {
            xlogger.entry();
            logger.info("trying init");
            status = "trying to init";
            final ProxyAttribute ap = new ProxyAttribute(enumAttributeName, attributeName, false, true);
            dynMgnt.addAttribute(ap);
            for (int i = 0; i < enumeratedLabelList.length; i++) {
                final String name = enumeratedLabelList[i].trim();
                // First version there is no comma separator
                final StringTokenizer tokens = new StringTokenizer(name, ",");
                String attrName = name;
                String commandName = name;
                if (tokens.countTokens() == 2) {
                    attrName = tokens.nextToken().trim();
                    commandName = tokens.nextToken().trim();
                }// Create the associated command name that activate the
                // attribute
                // if the boolean begin with is
                else {
                    if (commandName.startsWith("is") || commandName.startsWith("Is")) {
                        commandName = commandName.substring(2);
                    }
                    // fix for isClosed that was creating a "Clos" command
                    if (commandName.equalsIgnoreCase("Closed")) {
                        commandName = "Close";
                    } else if (commandName.endsWith("ed")) {
                        commandName = commandName.substring(0, commandName.length() - 2);
                    }
                }
                double accuracy;
                if (enumeratedAccuracyList.length == 1) {
                    accuracy = enumeratedAccuracyList[0];
                } else {
                    accuracy = enumeratedAccuracyList[i];
                }
                // Put the first character in UpperCase

                final String fistCharactere = commandName.substring(0, 1).toUpperCase();
                commandName = fistCharactere + commandName.substring(1);

                enumMap.put(attrName, new ImmutablePair<Double, Double>(enumeratedValueList[i], accuracy));
                final EnumDynamicAttribute da = new EnumDynamicAttribute(attrName, enumeratedValueList[i], accuracy,
                        attributeName, modulo);
                enumStateMap.put(da, DeviceState.valueOf(enumeratedStateList[i].toUpperCase()));
                dynMgnt.addAttribute(da);

                final EnumDynamicCommand dc = new EnumDynamicCommand(commandName, da);
                dynMgnt.addCommand(dc);
            }
            if (!stopCommandFullName.isEmpty()) {
                final ProxyCommand pc = new ProxyCommand(STOP_COMMAND_NAME, stopCommandFullName);
                dynMgnt.addCommand(pc);
            }
            xlogger.exit();
        }

        @Override
        public void getFallback() throws DevFailed {
            logger.info("init failure fallback");
            dynMgnt.clearAttributesWithExclude("log");
        }

        @Override
        public void notifyError(final DevFailed e) {
            initOK = false;
            final StringBuilder sb = new StringBuilder();
            sb.append("INIT FAILED, will retry in a while\n").append(DevFailedUtils.toString(e));
            status = sb.toString();
            logger.error("{}", status);
        }

    }

}
