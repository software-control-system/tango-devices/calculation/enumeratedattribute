package fr.soleil.tango.server.enumeratedattribute;

import org.slf4j.ext.XLogger;
import org.slf4j.ext.XLoggerFactory;
import org.tango.server.StateMachineBehavior;
import org.tango.server.command.CommandConfiguration;
import org.tango.server.command.ICommandBehavior;

import fr.esrf.Tango.DevFailed;
import fr.esrf.Tango.DispLevel;

/**
 * 
 * @author fourneau
 * 
 */
public final class EnumDynamicCommand implements ICommandBehavior {
    // private static Logger logger = LoggerFactory.getLogger(EnumDynamicCommand.class);
    private static final XLogger XLOGGER = XLoggerFactory.getXLogger(EnumDynamicCommand.class);
    private final EnumDynamicAttribute dynamicAttribute;
    private final CommandConfiguration config = new CommandConfiguration();

    /**
     * Constructor
     * 
     * @param commandName
     * @param da
     * @throws DevFailed
     */
    public EnumDynamicCommand(final String commandName, final EnumDynamicAttribute da) throws DevFailed {
	XLOGGER.entry();
	config.setInType(void.class);
	config.setOutType(void.class);
	config.setName(commandName);
	config.setDispLevel(DispLevel.OPERATOR);
	dynamicAttribute = da;
	XLOGGER.exit();
    }

    /**
     * execute command
     * 
     * @return value
     * @see org.tango.server.command.ICommandBehavior#execute(java.lang.Object)
     */
    @Override
    public Object execute(final Object arg0) throws DevFailed {
	XLOGGER.entry();
	dynamicAttribute.writeValueOnProxy();
	XLOGGER.exit();
	return null;
    }

    /**
     * return config
     * 
     * @return config
     * @see org.tango.server.command.ICommandBehavior#getConfiguration()
     */
    @Override
    public CommandConfiguration getConfiguration() throws DevFailed {
	XLOGGER.entry();
	XLOGGER.exit();
	return config;
    }

    /**
     * @return null
     * @see org.tango.server.command.ICommandBehavior#getStateMachine()
     */
    @Override
    public StateMachineBehavior getStateMachine() throws DevFailed {
	XLOGGER.entry();
	XLOGGER.exit();
	return null;
    }

    public boolean checkValue() throws DevFailed {
	return dynamicAttribute.isSelected();
    }

    /**
     * return the name of the command
     * 
     * @return name
     */
    public String getName() {
	XLOGGER.entry();
	XLOGGER.exit();
	return config.getName();
    }

}
