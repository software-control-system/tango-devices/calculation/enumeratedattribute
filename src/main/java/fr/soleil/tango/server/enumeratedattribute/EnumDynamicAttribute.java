package fr.soleil.tango.server.enumeratedattribute;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.ext.XLogger;
import org.slf4j.ext.XLoggerFactory;
import org.tango.server.StateMachineBehavior;
import org.tango.server.attribute.AttributeConfiguration;
import org.tango.server.attribute.AttributePropertiesImpl;
import org.tango.server.attribute.AttributeValue;
import org.tango.server.attribute.IAttributeBehavior;
import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.AttrWriteType;
import fr.esrf.Tango.DevFailed;
import fr.soleil.tango.clientapi.TangoAttribute;

/**
 * @author fourneau
 *
 */
public final class EnumDynamicAttribute implements IAttributeBehavior {

    private final Logger logger = LoggerFactory.getLogger(EnumeratedAttribute.class);
    private static final XLogger XLOGGER = XLoggerFactory.getXLogger(EnumDynamicAttribute.class);
    private final double accuracy;
    private final TangoAttribute attr;
    private final AttributeConfiguration config;
    private final int modulo;
    private final AttributeValue value = new AttributeValue();
    private final double valueToWrite;

    /**
     * constructor
     *
     * @param attrName
     * @param valueToWrite
     * @param accuracy
     * @param proxy
     * @param modulo
     * @throws DevFailed
     */
    public EnumDynamicAttribute(final String attrName, final double valueToWrite, final double accuracy,
            final String proxy, final int modulo) throws DevFailed {
        XLOGGER.entry();
        attr = new TangoAttribute(proxy);
        if (!attr.isScalar()) {
            DevFailedUtils.throwDevFailed("CONFIG_ERROR", proxy + " is not scalar");
        }
        config = new AttributeConfiguration();
        config.setName(attrName);
        config.setType(boolean.class);
        final AttributePropertiesImpl props = new AttributePropertiesImpl();
        props.setDescription("enum value for " + proxy + " is " + valueToWrite + "+/-" + accuracy);
        config.setAttributeProperties(props);
        config.setWritable(AttrWriteType.READ_WRITE);
        this.valueToWrite = valueToWrite;
        this.modulo = modulo;
        this.accuracy = accuracy;
        XLOGGER.exit();

    }

    /**
     * return true if the value is between low and high Limit
     *
     * @return true if the value is between low and high Limit
     * @throws DevFailed
     */
    public boolean isSelected() throws DevFailed {
        XLOGGER.entry();
        final double readValue = attr.read(double.class);
        boolean isValid = false;

        double rValue = readValue;
        double wValue = valueToWrite;
        if (modulo != 0) {
            rValue = readValue % modulo;
            // System.out.println(rValue);
            wValue = valueToWrite % modulo;
        }
        final double lowLimit = wValue - accuracy;
        final double hightLimit = wValue + accuracy;

        if (rValue >= lowLimit && rValue <= hightLimit) {
            // System.out.println(lowLimit + "<" + readValue + "<" +
            // hightLimit);
            isValid = true;
        }
        // else {
        // System.out.println("OutSide " + lowLimit + "<" + readValue + "<" +
        // hightLimit);
        // }

        XLOGGER.exit();
        return isValid;
    }

    /**
     * Recalculate value if a modulo is defined.
     *
     * @param actualValue
     * @return
     */
    private double computeEnumeratedValue(final double actualValue) {
        double setValue = valueToWrite;
        if (modulo != 0) {
            // take only the absolute value
            int writeValue = Double.valueOf(Math.abs(setValue)).intValue();
            int readValue = Double.valueOf(Math.abs(actualValue)).intValue();

            // Calculate the coordinate in positive value
            writeValue = writeValue % modulo;
            if (setValue < 0) {
                writeValue = modulo - writeValue;
            }
            readValue = readValue % modulo;
            if (actualValue < 0) {
                readValue = modulo - readValue;
            }
            // check the difference
            final int diffValue = Math.abs(readValue - writeValue);
            if (diffValue > modulo / 2) {
                writeValue = writeValue - modulo;
            }

            setValue = writeValue;
        }
        XLOGGER.exit();
        return setValue;
    }

    /**
     * return config
     *
     * @return config
     * @see org.tango.server.attribute.IAttributeBehavior#getConfiguration()
     */
    @Override
    public AttributeConfiguration getConfiguration() throws DevFailed {
        XLOGGER.entry();
        XLOGGER.exit();
        return config;
    }

    /**
     * return the name of the attribute
     *
     * @return name
     */
    public String getName() {
        XLOGGER.entry();
        XLOGGER.exit();
        return config.getName();
    }

    /**
     * @return null
     * @see org.tango.server.attribute.IAttributeBehavior#getStateMachine()
     */
    @Override
    public StateMachineBehavior getStateMachine() throws DevFailed {
        XLOGGER.entry();
        XLOGGER.exit();
        return null;
    }

    /**
     * get attribute value's
     *
     * @return AttributeValue
     * @see org.tango.server.attribute.IAttributeBehavior#getValue()
     */
    @Override
    public AttributeValue getValue() throws DevFailed {
        XLOGGER.entry();
        value.setValue(isSelected());
        XLOGGER.exit();
        return value;
    }

    /**
     * set attribute value's
     *
     * @param arg0
     * @see org.tango.server.attribute.IAttributeBehavior#setValue()
     */
    @Override
    public void setValue(final AttributeValue arg0) throws DevFailed {
        XLOGGER.entry();
        final boolean applyEnumValue = (Boolean) arg0.getValue();
        if (applyEnumValue) {
            writeValueOnProxy();
        }
        XLOGGER.exit();
    }

    /**
     * write value on the proxy
     *
     * @throws DevFailed
     */
    public void writeValueOnProxy() throws DevFailed {
        XLOGGER.entry();
        final double readValue = attr.read(double.class);
        if (modulo != 0) {
            final boolean computeModulo = readValue % modulo != valueToWrite;
            if (computeModulo) {
                final double moduloValue = computeEnumeratedValue(readValue);
                logger.info("writing {} on {}", moduloValue, attr.getName());
                attr.write(moduloValue);
            }
        } else {
            if (valueToWrite != readValue) {
                final double moduloValue = computeEnumeratedValue(readValue);
                logger.info("writing {} on {}", moduloValue, attr.getName());
                attr.write(moduloValue);
            }
        }
        XLOGGER.exit();
    }
}
